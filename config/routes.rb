Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth', controllers: {
    sessions:  'overrides/sessions'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope module: 'api' do
    namespace :v1 do
      resources :users, only: [:index, :show] do
        collection do
          post :changeUserRoleToHMO
          post :createAppointment
          post :updateAppointment
        end
      end
      resources :hmos do
        member do
          post :addMedicalStablishment
          delete :removeMedicalStablishment
          post :addDoctor
          post :removeDoctor
          get :allDoctors
        end
      end

      resources :doctors, only:[:index, :create, :update, :show]
      resources :medical_stablishments, except: [:destroy, :index] do
        collection do
          get :getNearbyStablishment
          get :searchByCity
          post :updateCheckupStatus
          post :addDoctorToClinic
          post :changeScheduleOfDoctor
          get :allDoctorsFromClinic
          post :removeDoctorFromClinic
          get :listPatientScheduled
        end
      end
    end
  end
end
