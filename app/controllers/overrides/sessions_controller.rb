module Overrides
  class SessionsController < DeviseTokenAuth::SessionsController

    def render_create_success
      # @resource will have been set by set_user_by_token concern
      if @resource

        render json: {
          data: @resource,
          role: @resource.roles
        }
      else
        render json: {
          success: false,
          errors: ["Invalid login credentials"]
        }, status: 401
      end
    end
  end
end