module Api::V1
  class ApiController < ApplicationController
    before_action :authenticate_user!
    load_and_authorize_resource
    respond_to :json
  end
end