module Api::V1
  class MedicalStablishmentsController < ApiController
    before_action :stablishment_owner, only:[
                                              :update, 
                                              :update_checkup_status, 
                                              :updateCheckupStatus,
                                              :changeScheduleOfDoctor,
                                              :allDoctorsFromClinic,
                                              :removeDoctorFromClinic,
                                              :listPatientScheduled,
                                              :addDoctorToClinic]

    def create
      stablishment = MedicalStablishment.new medical_stablishment_params
      if stablishment.save
        @current_user.medical_stablishment = stablishment
        
        if @current_user.save
          render json: { data: stablishment, status: 201 }
        else
          render json: { error: stablishment.errors.full_messages.first }, status: :unauthorized
        end
      else
        render json: { error: stablishment.errors.full_messages.first }, status: :unauthorized
      end
    end

    def show
      if params[:id].present?
        medStablishment = MedicalStablishment.find(params[:id])
        render json: medStablishment
     else
        render json: { error: 'Access Denied' }, status: :unauthorized
     end
    end

    def update
      if params[:id].present?
         medStablishment = @current_user.medical_stablishment
         if medStablishment != nil && medStablishment.update(medical_stablishment_params)
          render json: medStablishment
         end
          render json: { error: medStablishment.errors.full_messages.first }, status: :unauthorized
      else
        render json: { error: 'Access Denied' }, status: :unauthorized
      end
    end

# Start Doctors Methods
    def addDoctorToClinic
      clinic = @current_user.medical_stablishment
      selectedDoctor = Doctor.find(params[:doctor_id])
      isExist = MedicalStablishmentDoctor.where(doctor: selectedDoctor, medical_stablishment: clinic).count
      if selectedDoctor.present? && isExist == 0 #meaning 
        docSched = MedicalStablishmentDoctor.new
        docSched.medical_stablishment = clinic
        docSched.doctor = selectedDoctor
        docSched.schedule = params[:schedule]
        if docSched.save
          render json: { data: docSched, status: 201 }
        else
          render json: { error: 'Acess Denied' }, status: :unauthorized
        end
      else
        render json: { error: 'Record already exist' }, status: :unauthorized
      end
    end

    def changeScheduleOfDoctor
      clinic = @current_user.medical_stablishment
      selectedSchedule = MedicalStablishmentDoctor.where(doctor_id: params[:doctor_id], id: params[:schedule_id]).first
      if !selectedSchedule.present?
        render json: { error: 'Acess Denied' }, status: :unauthorized
        return false
      end

      selectedSchedule.schedule = params[:schedule]
      
      if selectedSchedule.save
        render json: selectedSchedule, status: 201
      else
        render json: { error: 'Acess Denied' }, status: :unauthorized
      end
    end

    def allDoctorsFromClinic
      clinic = @current_user.medical_stablishment
      doctors = clinic.doctors.joins(:medical_stablishment_doctors).where(medical_stablishment_doctors: {is_affiliated: true})

      if doctors.length > 0
        render json: { data: doctors, status: 200 }
      else
        render json: { data: [], status: 200 }
      end
    end

    def removeDoctorFromClinic
      clinic = @current_user.medical_stablishment
      selectedSchedule = MedicalStablishmentDoctor.where(doctor_id: params[:doctor_id], id: params[:schedule_id]).first
      if !selectedSchedule.present?
        render json: { error: 'Acess Denied' }, status: :unauthorized
        return false
      end

      selectedSchedule.is_affiliated = false
      
      if selectedSchedule.save
        render json: selectedSchedule, status: 201
      else
        render json: { error: 'Acess Denied' }, status: :unauthorized
      end
    end
# End Doctors Methods



    def listPatientScheduled
      # if @current_user.type == 'Patient'
      #   id = params[:id].to_i
      #   medicalStablishment = MedicalStablishment.find(id)
      #   if medicalStablishment.present?
      #     checkup_count = ScheduleCheckup.where(
      #       medical_stablishment: medicalStablishment, 
      #       scheduled_date_time: Date.today.all_day, 
      #       checkup_status: 0).count # 0 means 
      #     render json: { schedule_count: checkup_count }  
      #   end

      # elsif @current_user.type = 'ClinicOwner'
        
      # end

      medicalStablishment = @current_user.medical_stablishment
      if medicalStablishment.present?
        # patient_list = ScheduleCheckup.where(
        #   medical_stablishment: medicalStablishment, 
        #   scheduled_date_time: Date.today.all_day)
          patient_list = User.select('
            schedule_checkups.user_id, 
            users.first_name, 
            users.last_name, 
            users.contact_number, 
            users.email, 
            schedule_checkups.priority_number,
            schedule_checkups.scheduled_date_time, 
            schedule_checkups.checkup_status,
            schedule_checkups.id').joins(:schedule_checkups).where(schedule_checkups: 
          { medical_stablishment: medicalStablishment, scheduled_date_time: Date.today.all_day }).order('schedule_checkups.priority_number') 
        render json: patient_list
      end
    end

    def updateCheckupStatus
      schedule_id = params[:schedule_id]
      schedule_status = params[:schedule_status]

      schedule = ScheduleCheckup.where(id: schedule_id, medical_stablishment: @current_user)

      if schedule.present?
        schedule.checkup_status = schedule_status
        if schedule.save
           render json: schedule 
        else
          render json: { error: "No records found." }, status: :unauthorized
        end
      else
        render json: { error: "No records found." }, status: :unauthorized  
      end
    end

    def getNearbyStablishment
      radius = 5 #default radius of the area.
      current_user = @current_user
      stablishementList = []
      if params[:latitude].present? && params[:longitude].present?

        latitude = params[:latitude].to_f
        longitude = params[:longitude].to_f
        
        if @current_user.hmo != nil
          hmo = @current_user.hmo
          med_stablishments = hmo.medical_stablishments.near([latitude,longitude], radius)
        else
          med_stablishments = MedicalStablishment.near([latitude,longitude], radius)
        end


        render json: [] unless med_stablishments.size > 0

        med_stablishments.each do | medical_stablishment |
          med_doc = {
            :medical_stablishment => medical_stablishment,
            :doctors => get_doctors(medical_stablishment)
          }

          stablishementList.push(med_doc)
          render json: stablishementList 
        end
        
      else
        
      end
    end

    def searchByCity
      current_user = @current_user
      if params[:city].present? && (current_user.has_role? :patient)
        if current_user.hmo != nil
          hmo = current_user.hmo
          med_stablishments = hmo.medical_stablishments.where("city = ?", params[:city].downcase) 
        else
          med_stablishments = MedicalStablishment.where("city = ?", params[:city].downcase)
        end
        render json: med_stablishments
      end
    end

    private

    def stablishment_owner
        render json: {error: 'Acess Denied'}, status: :unauthorized unless @current_user.type == 'ClinicOwner'
    end

    def medical_stablishment_params
        params.require(:medical_stablishment).permit(
          :stablishment_name, 
          :bldg_name, 
          :street_address, 
          :city, 
          :province, 
          :country,
          :stablishment_type,
          :contact_number)
    end

    def get_doctors(medical_stablishment)
      doctors = Doctor.select('
        doctors.id,
        doctors.first_name, 
        doctors.last_name, 
        doctors.extension, 
        doctors.prc_number,
        doctors.main_specialty,
        doctors.other_specialty,
        medical_stablishment_doctors.schedule').joins(:medical_stablishment_doctors)
          .where(
            medical_stablishment_doctors: 
            {medical_stablishment: medical_stablishment})
    end
  end
end

