module Api::V1
  class DoctorsController < ApiController
    before_action :set_doctor, only: [:show, :update]

    def index
      doc_list = Doctor.all
      render json: { doctors: doc_list, status: 200 }
    end

    def create
      doctor = Doctor.new doctor_params
      
      if doctor.save
        render json: doctor
      else
        render json: { error: doctor.errors.full_messages.first }, status: :unauthorized
      end
    end

    def update
      if @@doctor.update(doctor_params)
        render json: @@doctor
      else
        render json: { error: @@doctor.errors.full_messages.first }, status: :unauthorized
      end
    end

    def show
      render json: @@doctor
    end

    private

    def doctor_params
      params.require(:doctor).permit(:first_name,:last_name,:extension,:prc_number,:first_name,:orginal_date_issue,:main_specialty,:other_specialty)
    end

    def set_doctor
      @@doctor = Doctor.find(params[:id])
    end
  end
end