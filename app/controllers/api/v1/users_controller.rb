module Api::V1
  class UsersController < ApiController
    # GET /v1/users
    def index
      render json: User.all
    end

    # GET /v1/users/{id}
    def show
      render json: @current_user
    end

    def createAppointment
      if @current_user.type == 'Patient'
        stablishment_id = params[:medical_stablishment_id]
        schedule = params[:scheduled_date_time]
        stablishment = MedicalStablishment.find(stablishment_id)

        appointment = ScheduleCheckup.new
        appointment.medical_stablishment = stablishment
        appointment.user = @current_user
        appointment.scheduled_date_time = schedule

        if appointment.save
          render json: appointment
        else
          render json: { error: appointment.errors.full_messages.first }, status: :unauthorized
        end
      else
        render json: { error: 'Access Denied' }, status: :unauthorized
      end 
    end

    def updateAppointment
      if @current_user.type == 'Patient'

        schedule_id = params[:schedule_id].to_i
        patient_schedule = ScheduleCheckup.find(schedule_id)

        if patient_schedule.present?
          status = 'cancelled' # cancelled appointment

          patient_schedule.checkup_status = status
          
          if patient_schedule.save
            render json: patient_schedule
          else
            render json: { error: 'User is not authorized' }, status: :unauthorized
          end
        else
          render json: { error: 'User is not authorized' }, status: :unauthorized
        end
      else
        render json: { error: 'User is not authorized' }, status: :unauthorized
      end
    end

    def changeUserRoleToHMO
      userHMO = User.find(params[:id])
      
      if(userHMO.remove_role :user)
        if userHMO.add_role :hmo_owner
          render json: userHMO
        else
          render json: { error: 'User is not authorized' }, status: :unauthorized
        end
      else
        render json: { error: 'User is not authorized' }, status: :unauthorized
      end
    end

    private

    def checkup_appointment_params
      params.require(:schedule_checkup).permit(
        :medical_stablishment_id, 
        :scheduled_date_time
        )
    end
  end
end