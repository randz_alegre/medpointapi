module Api::V1
  class HmosController < ApiController
    before_action :get_owner, except: [:show]
    before_action :set_user_hmo, only: [:addDoctor, :removeDoctor, :allDoctors, :addMedicalStablishment, :removeMedicalStablishment]
    # GET /v1/hmos
    def index
      render json: Hmo.all
    end

    def create
      hmo = Hmo.new hmo_params

      if hmo.save
        render json: hmo
      else
        render json: { error: hmo.errors.full_messages.first }, status: :unauthorized
      end
    end
    # GET /v1/hmos/{id}
    def show
      render json: Hmo.find(params[:id])
    end

    def update
      if @@hmo.update(hmo_params)
        render json: @@hmo
      else
        render json: { error: @@hmo.errors.full_messages.first }, status: :unauthorized
      end
    end

    def addMedicalStablishment
      if params[:medical_stablishment].present?
        medicalStablishment = MedicalStablishment.find(params[:medical_stablishment])
        hmoStablishment = HmoStablishment.find_or_create_by(hmo: @@hmo, medical_stablishment: medicalStablishment, is_deleted: false)
        hmoStablishment.coordinator_first_name = params[:coordinator_first_name]
        hmoStablishment.coordinator_last_name = params[:coordinator_last_name]
        hmoStablishment.contact_number = params[:contact_number]
        hmoStablishment.industrial_section = params[:industrial_section]
        if hmoStablishment.save
          render json: { data: hmoStablishment, status: 201 }
        end
      else
        render json: { error: 'Acess Denied' }, status: :unauthorized
      end
      
    end

    def removeMedicalStablishment
      if params[:medical_stablishment].present?
        medicalStablishment = MedicalStablishment.find(params[:medical_stablishment])
        hmoStablishment = HmoStablishment.where(hmo:@@hmo, medical_stablishment: medicalStablishment).first
        hmoStablishment.is_deleted = true
        
        if hmoStablishment.save
          render json: { data: "Successfully deleted.", status: 201 }
        else
          render json: { error: 'Acess Denied' }, status: :unauthorized
        end
      end
    end

    def addDoctor
      userHmo = @@hmo
      
      selectedDoctor = Doctor.find(params[:doctor_id])
      record = HmoDoctor.where(hmo:userHmo, doctor:selectedDoctor).first

      if selectedDoctor && !record
       hmoDoctor = HmoDoctor.new
       hmoDoctor.hmo = @@hmo
       hmoDoctor.doctor = selectedDoctor 
       if hmoDoctor.save
         render json: { data: userHmo.doctors, status: 201 }
       else
         render json: { error: 'Acess Denied' }, status: :unauthorized
       end
     else
       render json: { error: 'Record already exist' }, status: :unauthorized
      end
    end

    def allDoctors
      userHmo = @@hmo
      doctors = Doctor.joins(:hmo_doctors).where(hmo_doctors: {is_deleted: true, hmo: userHmo }).distinct.pluck
  
      if doctors.length > 0
        render json: { data: doctors, status: 200 }
      else
        render json: { error: 'Record not found' }, status: :unauthorized
      end
    end

    def removeDoctor
      userHmo = @@hmo
      doctor = Doctor.find(params[:doctor_id])
      if doctor
        selectedDoctor = HmoDoctor.where(hmo:userHmo, doctor:doctor).first
        if selectedDoctor
          selectedDoctor.is_deleted = true
          if selectedDoctor.save
            render json: { data: selectedDoctor, status: 201 }
          else
            render json: { error: 'Access Denied' }, status: :unauthorized
          end
        else
          
        end
      else
        render json: { error: 'Record not found' }, status: :unauthorized
      end
      
    end
    private

    def hmo_params
      params.require(:hmo).permit(:hmo_name, :address, :contact_number, :trunkline, :mic, :toll_free)
    end

    def set_user_hmo
      @@hmo = @current_user.hmo
    end
    
    def get_owner
      render json: {error: 'Acess Denied'}, status: :unauthorized unless @current_user.type == 'HmoOwner' # Owner type
    end
  end
end