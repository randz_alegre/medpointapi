class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from CanCan::AccessDenied, with: :access_denied

  def record_not_found
    render json: { error: 'Record not found' }, status: :unauthorized
  end

  def access_denied
    render json: { error: 'Acess Denied' }, status: :unauthorized
  end
    
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :contact_number])
    end
end
