class Hmo < ApplicationRecord
  resourcify
  # == Schema Information
  #
  # Table name: Hmos
  # t.string    :hmo_name
  # t.string    :address
  # t.string    :contact_number
  # t.string    :trunkline
  # t.string    :mic
  # t.string    :toll_free
  # t.string    :avatar_filename
  # t.integer   :avatar_file_size
  # t.datetime  :avatar_uploaded_at
  belongs_to :user #This is the admin of the HMO
  has_many :users, ->{ where type: 'Patient' }, class_name: 'User'  #0 for Users 1 for Owner
  has_many :hmo_stablishments
  has_many :medical_stablishments, through: :hmo_stablishments

  has_many :hmo_doctors
  has_many :doctors, through: :hmo_doctors

  validates :hmo_name, presence: true
  validates :address, presence: true
  validates :contact_number, presence: true
  validates :trunkline, presence: true
  
end
