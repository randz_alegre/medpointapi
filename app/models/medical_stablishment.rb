class MedicalStablishment < ApplicationRecord
  belongs_to :user, ->{ where type: 'ClinicOwner' }, class_name: 'ClinicOwner'
  #MedicalStablishment User Relationship
  has_many :schedule_checkups
  has_many :users, ->{ where type: 'Patient' }, class_name: 'Patient' , through: :schedule_checkups 

  #MedicalStablishment Doctor relationship
  has_many :medical_stablishment_doctors
  has_many :doctors, through: :medical_stablishment_doctors
  
  #MedicalStablishment Hmo relationship
  has_many :hmo_stablishments
  has_many :hmos, through: :hmo_stablishments

  geocoded_by :stablishment_address
  validates :stablishment_name, presence: true, length: { maximum: 50 }	
  validates :bldg_name, presence: true, length: { maximum: 200 }	
  validates :city, presence: true, length: { maximum: 50 }	
  validates :province, presence: true, length: { maximum: 50 }	
  validates :contact_number, presence: true, length: { maximum: 200 }	
  validates :street_address, presence: true, length: { maximum: 200 }	
  validates :country, presence: true, length: { maximum: 50 }
  validates :stablishment_type, presence: true, length: { maximum: 50 }		

  before_save :downcase_fields
  after_validation :geocode

  def downcase_fields
    self.street_address.downcase!
    self.city.downcase!
    self.province.downcase!
  end

  def stablishment_address
    [bldg_name, street_address, city, province, country].compact.join(', ')
  end
end
