#checkup_status = [pending, accepted, done, cancelled] 0, 1, 2, 3

class ScheduleCheckup < ApplicationRecord
  belongs_to :medical_stablishment
  belongs_to :user, ->{ where type: 'Patient' }, class_name: 'User'

  include AASM

  aasm column: 'checkup_status' do
    state :pending, initial: true
    state :accepted, :done, :cancelled

    event :acknowledge do
      transitions from: [:pending], to: :accepted
    end

    event :reject do
      transitions from: [:accepted], to: :cancelled
    end

    event :verified do
      transitions from: [:accepted], to: :done
    end

    event :reset do
      transitions from: [:accepted, :cancelled], to: :pending
    end
  end
end
