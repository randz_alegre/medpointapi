class Doctor < ApplicationRecord
  has_many :medical_stablishment_doctors
  has_many :medical_stablishments, through: :medical_stablishment_doctors

  has_many :hmo_doctors
  has_many :hmos, through: :hmo_doctors

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :extension, presence: true
  validates :prc_number, presence: true
  validates :orginal_date_issue, presence: true
  validates :main_specialty, presence: true
  validates :other_specialty, presence: true
end
