class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities

    if user.has_role? :admin
      can :create, Hmo
      can :read, User
      can :create, MedicalStablishment
      can :getNearbyStablishment, MedicalStablishment
      can :changeUserRoleToHMO, User
      can :manage, Doctor
    end

    if user.has_role? :hmo_owner
      can :read, Hmo
      can :update, Hmo
      can :addMedicalStablishment, Hmo
      can :removeMedicalStablishment, Hmo
      can :addDoctor, Hmo
      can :allDoctors, Hmo
      can :removeDoctor, Hmo
      can :create, MedicalStablishment
      can :manage, Doctor
    end

    if user.has_role? :medical_stablishment_owner
      can :manage, Doctor
      can :update, MedicalStablishment
      can :show, MedicalStablishment
      can :updateAppointment, User
      can :addDoctorToClinic, MedicalStablishment
      can :changeScheduleOfDoctor, MedicalStablishment
      can :allDoctorsFromClinic, MedicalStablishment
      can :removeDoctorFromClinic, MedicalStablishment
    end
    
    if user.has_role? :patient
      can :show, Hmo
      can :read, Hmo
      can :show, Doctor
      can :createAppointment, User
      can :updateAppointment, User
      can :show, User
      can :getNearbyStablishment, MedicalStablishment
      can :searchByCity, MedicalStablishment
      can :read, MedicalStablishment
      can :listPatientScheduled, MedicalStablishment
    end
  end
end
