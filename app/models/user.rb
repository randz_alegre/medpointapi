# User Type : Patient, ClinicOwner, HmoOwner
class User < ActiveRecord::Base
  has_one :hmo
  belongs_to :hmo
  belongs_to :medical_stablishment

  has_many :schedule_checkups
  has_many :medical_stablishments, through: :schedule_checkups
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User
  
  after_commit :set_default_role
  

  def set_default_role
    self.add_role :patient
  end
end
