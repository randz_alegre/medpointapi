class ClinicJob < ApplicationJob
  queue_as :default

  def perform(targetChannel, data)
    ActionCable.server.broadcast(targetChannel, data)
  end
end
