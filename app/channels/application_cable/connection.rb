module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user
 
    def connect
      params = request.query_parameters()
      access_token = params['access-token']
      client = params['client']
      uid = params['uid']
      self.current_user = find_verified_user(access_token, client, uid)
      logger.add_tags 'ActionCable', current_user.email
    end
 
    private
      def find_verified_user(access_token, client, uid)
        user = User.find_by(email: uid)
        if user && user.valid_token?(access_token, client)
          user
        else
          reject_unauthorized_connection
        end
      end
  end
end