
class MedicalStablishmentChannel < ActionCable::Channel::Base
  def subscribed
    if current_user.type == 'ClinicOwner'
      @clinic_stream_name = current_user.medical_stablishment.stablishment_name.gsub ' ', '_'
      @clinic_stream_all = @clinic_stream_name + "_" + current_user.medical_stablishment.id.to_s
      @doc_owner = current_user.becomes(ClinicOwner)
      @clinic_stream = @clinic_stream_name + "_" + current_user.medical_stablishment.id.to_s + '_' + current_user.id.to_s
      stream_from @clinic_stream #private stream of clinic

    else
      clinicDetails = params[:clinicSubscription]
      @clinic = MedicalStablishment.find(clinicDetails[:id])
      @user_stream = current_user.email+ '_' + current_user.id.to_s
      clinic_stream_text = (@clinic.stablishment_name.gsub ' ', '_')
      @clinic_stream_all_from_client = clinic_stream_text + "_" + @clinic.id.to_s
      @clinicOwner = User.where("type = ? AND medical_stablishment_id = ?", "ClinicOwner", @clinic).limit(1)

      @clientClinicStream = clinic_stream_text + "_" + @clinic.id.to_s + '_' + @clinicOwner.first.id.to_s
      stream_from @user_stream
      stream_from @clinic_stream_all_from_client
    end
  end

  def approveAppointment(data)
    appointmentDetails = data['appointmentDetails']

    userAppointment = ScheduleCheckup.where("user_id = ? AND id = ? AND checkup_status = ? AND scheduled_date_time = ?", 
      appointmentDetails['user_id'], appointmentDetails['id'], "pending", appointmentDetails['scheduled_date_time'])
    
    if userAppointment.length
      puts userAppointment.inspect
      appointment = userAppointment[0]
      appointment.acknowledge
      appointment.priority_number = getAppointmentListForToday
      if appointment.save
        sendTotalAppointment
        allAppointmentsForToday = User.select('
          schedule_checkups.user_id, 
          users.first_name, 
          users.last_name, 
          users.contact_number, 
          users.email, 
          schedule_checkups.priority_number,
          schedule_checkups.scheduled_date_time, 
          schedule_checkups.checkup_status,
          schedule_checkups.id').joins(:schedule_checkups).where(schedule_checkups: 
        { medical_stablishment: @doc_owner.medical_stablishment, scheduled_date_time: Date.today.all_day }).order('schedule_checkups.priority_number') 
          resData = {
            "allAppointmentsForToday" => {
              "list" => allAppointmentsForToday
            }
          } 
        ActionCable.server.broadcast(@clinic_stream,resData.to_json)  
      end
    end
    

  end

  def sendTotalAppointment
    resData = {
      "clinic" => @doc_owner.medical_stablishment.stablishment_name,
      "id" => @doc_owner.medical_stablishment.id,
      "priority_number" => getAppointmentListForToday
    }
    ActionCable.server.broadcast(@clinic_stream_all, resData.to_json)
  end

  def setAppointment(data)
    res =  data['appointment_details']
    user = current_user
    appointment_exist = ScheduleCheckup.where("user_id = ? AND medical_stablishment_id = ? AND scheduled_date_time = ? OR (checkup_status = ? OR checkup_status = ?)", 
      current_user, @clinic, res['schedule_date'], "pending", "accepted")
    resData = nil

    if user && appointment_exist.empty?
      newAppointment = ScheduleCheckup.new
      newAppointment.priority_number = 0
      newAppointment.user = user
      newAppointment.medical_stablishment = @clinic
      newAppointment.scheduled_date_time = res['schedule_date']

      if newAppointment.save
        appointment = {
          'priority_number' => 0,
          'first_name' => user.first_name,
          'last_name' => user.last_name,
          'contact_number' => user.contact_number,
          'email' => user.email,
          'user_id' => user.id,
          'id' => newAppointment.id,
          'scheduled_date_time' => res['schedule_date'],
          'checkup_status' => newAppointment.checkup_status
        }
        resData = {
          'appointmentDetails' => appointment
        }
        ClinicJob.perform_later(@clientClinicStream, resData.to_json)
      end
    else
      resData = {
        'appointmentDetails' => {
          'error' => true,
          'message' => "Appointment already exist."
        }
      }
      ClinicJob.perform_later(@user_stream, resData.to_json)
    end
    
  end

  private

  def getAppointmentListForToday
    result = ScheduleCheckup.where("medical_stablishment_id = ? AND checkup_status = ? AND scheduled_date_time = ?", 
      @doc_owner.medical_stablishment.id, 'accepted', Date.today).order(priority_number: :asc) #checkup_status 0 means pending
    
    if result.empty?
      return 1
    end

    if result.length
      last = result.last
      return last.priority_number.to_i + 1
    end
  end

  def insertSortAppointments(data)
    data.sort_by { |hash| hash['priority_number'] }
  end

  def sortListOfAppointmentPerDate(data)
    data.sort_by { |element| element.keys.first }
  end

  def getLatestPriorityNumber(list, index)
    if list[index]
      clinicAppointment = list[index]
      lastIndex = clinicAppointment.length - 1
      return clinicAppointment[lastIndex].priority_number
    else
      return 0
    end
  end
end
