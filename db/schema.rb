# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181105122430) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clinic_owners", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "doctors", force: :cascade do |t|
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.string "extension", null: false
    t.string "prc_number", null: false
    t.date "orginal_date_issue", null: false
    t.string "main_specialty"
    t.string "other_specialty"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hmo_doctors", force: :cascade do |t|
    t.bigint "hmo_id"
    t.bigint "doctor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_deleted", default: false
    t.index ["doctor_id"], name: "index_hmo_doctors_on_doctor_id"
    t.index ["hmo_id"], name: "index_hmo_doctors_on_hmo_id"
  end

  create_table "hmo_stablishments", force: :cascade do |t|
    t.bigint "hmo_id"
    t.bigint "medical_stablishment_id"
    t.string "coordinator_first_name"
    t.string "coordinator_last_name"
    t.string "contact_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "industrial_section"
    t.boolean "is_deleted", default: false
    t.index ["hmo_id"], name: "index_hmo_stablishments_on_hmo_id"
    t.index ["medical_stablishment_id"], name: "index_hmo_stablishments_on_medical_stablishment_id"
  end

  create_table "hmos", force: :cascade do |t|
    t.string "hmo_name"
    t.string "address"
    t.string "contact_number"
    t.string "trunkline"
    t.string "mic"
    t.string "toll_free"
    t.string "avatar_filename"
    t.integer "avatar_file_size"
    t.datetime "avatar_uploaded_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_hmos_on_user_id"
  end

  create_table "medical_stablishment_doctors", force: :cascade do |t|
    t.bigint "medical_stablishment_id"
    t.bigint "doctor_id"
    t.string "schedule"
    t.boolean "is_affiliated", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["doctor_id"], name: "index_medical_stablishment_doctors_on_doctor_id"
    t.index ["medical_stablishment_id"], name: "index_medical_stablishment_doctors_on_medical_stablishment_id"
  end

  create_table "medical_stablishments", force: :cascade do |t|
    t.string "stablishment_name"
    t.string "bldg_name"
    t.string "street_address"
    t.string "city"
    t.string "province"
    t.string "country"
    t.string "stablishment_type"
    t.string "contact_number"
    t.decimal "latitude"
    t.decimal "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "patients", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "schedule_checkups", force: :cascade do |t|
    t.bigint "medical_stablishment_id"
    t.bigint "user_id"
    t.datetime "scheduled_date_time", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "priority_number", null: false
    t.string "checkup_status"
    t.index ["medical_stablishment_id"], name: "index_schedule_checkups_on_medical_stablishment_id"
    t.index ["user_id"], name: "index_schedule_checkups_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.text "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "contact_number"
    t.bigint "hmo_id"
    t.bigint "medical_stablishment_id"
    t.string "type", default: "Patient"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["hmo_id"], name: "index_users_on_hmo_id"
    t.index ["medical_stablishment_id"], name: "index_users_on_medical_stablishment_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  add_foreign_key "hmos", "users"
  add_foreign_key "users", "hmos"
  add_foreign_key "users", "medical_stablishments"
end
