class ChangeOwnerToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :type, :integer, default: 0 # 0 user, 1 owner
    remove_column :hmo_doctors, :schedule, :string
    add_column :hmo_doctors, :is_deleted, :boolean, default: false #not deleted
  end
end
