class CreateHmoStablishments < ActiveRecord::Migration[5.1]
  def change
    create_table :hmo_stablishments do |t|
      t.references :hmo, index: true
      t.references :medical_stablishment, index: true
      t.string :coordinator_first_name
      t.string :coordinator_last_name
      t.string :contact_number
      t.timestamps
    end
  end
end
