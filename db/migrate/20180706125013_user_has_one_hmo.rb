class UserHasOneHmo < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :hmo, foreign_key: true, index: true
  end
end
