class CreateHmoDoctors < ActiveRecord::Migration[5.1]
  def change
    create_table :hmo_doctors do |t|
      t.references :hmo, index: true
      t.references :doctor, index: true
      t.string :schedule
      t.timestamps
    end
  end
end
