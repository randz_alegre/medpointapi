class CreateDoctors < ActiveRecord::Migration[5.1]
  def change
    create_table :doctors do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :extension, null: false
      t.string :prc_number, null: false
      t.date   :orginal_date_issue, null: false
      t.string :main_specialty
      t.string :other_specialty
      t.timestamps
    end
  end
end
