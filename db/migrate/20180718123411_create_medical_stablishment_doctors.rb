class CreateMedicalStablishmentDoctors < ActiveRecord::Migration[5.1]
  def change
    create_table :medical_stablishment_doctors do |t|
      t.references :medical_stablishment, index: true
      t.references :doctor, index: true
      t.string :schedule
      t.boolean :is_affiliated, default: true
      t.timestamps
    end
  end
end
