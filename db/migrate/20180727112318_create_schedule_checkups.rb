class CreateScheduleCheckups < ActiveRecord::Migration[5.1]
  def change
    create_table :schedule_checkups do |t|
      t.references :medical_stablishment, index: true
      t.references :user, index: true
      t.datetime :scheduled_date_time, null: false
      t.timestamps
    end
  end
end
