class AddColumnToHmoMedicalStablishment < ActiveRecord::Migration[5.1]
  def change
    add_column :hmo_stablishments, :industrial_section, :string
    add_column :hmo_stablishments, :is_deleted, :boolean, default: false
  end
end
