class CreateMedicalStablishments < ActiveRecord::Migration[5.1]
  def change
    create_table :medical_stablishments do |t|
      t.string :stablishment_name
      t.string :bldg_name
      t.string :street_address
      t.string :city
      t.string :province
      t.string :country
      t.string :stablishment_type
      t.string :contact_number
      t.decimal :latitude
      t.decimal :longitude
      t.timestamps
    end
  end
end
