class UpdateChekupStatusColumn < ActiveRecord::Migration[5.1]
  def up
    add_column :schedule_checkups, :checkup_status, :string
  end

  def down
    remove_column :schedule_checkups, :checkup_status, :string
  end
end
