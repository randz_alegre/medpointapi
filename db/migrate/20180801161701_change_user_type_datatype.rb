class ChangeUserTypeDatatype < ActiveRecord::Migration[5.1]
  def up
    remove_column :users, :user_type, :string
    add_column :users, :user_type, :string, default: 'Patient'
  end

  def down
    remove_column :users, :user_type, :string
  end
end
