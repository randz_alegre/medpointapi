class AddPriorityNumberToScheduleCheckup < ActiveRecord::Migration[5.1]
  def change
    add_column :schedule_checkups, :priority_number, :integer, null: false
  end
end
