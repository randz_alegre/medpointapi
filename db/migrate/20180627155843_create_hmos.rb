class CreateHmos < ActiveRecord::Migration[5.1]
  def change
    create_table :hmos do |t|
      t.string    :hmo_name
      t.string    :address
      t.string    :contact_number
      t.string    :trunkline
      t.string    :mic
      t.string    :toll_free
      t.string    :avatar_filename
      t.integer   :avatar_file_size
      t.datetime  :avatar_uploaded_at
      t.timestamps
    end
  end
end
