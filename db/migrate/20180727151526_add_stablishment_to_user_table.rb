class AddStablishmentToUserTable < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :medical_stablishment, foreign_key: true, index: true
  end
end
