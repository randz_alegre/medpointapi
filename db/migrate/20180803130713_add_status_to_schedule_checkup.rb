class AddStatusToScheduleCheckup < ActiveRecord::Migration[5.1]
  def change
    add_column :schedule_checkups, :checkup_status, :integer, default: 0  
  end
end
